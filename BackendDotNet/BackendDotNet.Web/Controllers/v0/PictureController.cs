﻿using BackendDotNet.Common.Entities;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using BackendDotNet.Database.Abstract;
using BackendDotNet.Database.Concrete;

namespace BackendDotNet.Web.Controllers.v0
{
    public class PictureController : ApiController
    {
        private readonly PictureRepository _repository;

        public PictureController(IDbConnector connector)
        {
            _repository = new PictureRepository(connector);
        }

        // GET api/picture
        public HttpResponseMessage Get()
        {
            return Request.CreateResponse(HttpStatusCode.MovedPermanently, "/api/v1/");
        }

        // GET api/picture/5
        public HttpResponseMessage Get(long id)
        {
            return Request.CreateResponse(HttpStatusCode.MovedPermanently, "/api/v1/");
        }

        // POST api/picture
        public HttpResponseMessage Post([FromBody]Picture value)
        {
            return Request.CreateResponse(HttpStatusCode.MovedPermanently, "/api/v1/");
        }

        // PUT api/picture/5
        public HttpResponseMessage Put(long id, [FromBody]Picture value)
        {
            return Request.CreateResponse(HttpStatusCode.MovedPermanently, "/api/v1/");
        }

        // DELETE api/picture/5
        public HttpResponseMessage Delete(long id)
        {
            return Request.CreateResponse(HttpStatusCode.MovedPermanently, "/api/v1/");
        }

        public HttpResponseMessage Options()
        {
            return Request.CreateResponse(HttpStatusCode.MovedPermanently, "/api/v1/");
        }
    }
}
