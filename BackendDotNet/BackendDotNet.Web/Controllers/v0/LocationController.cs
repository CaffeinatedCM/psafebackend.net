﻿using BackendDotNet.Common.Entities;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using BackendDotNet.Database.Abstract;
using BackendDotNet.Database.Concrete;

namespace BackendDotNet.Web.Controllers.v0
{
    public class LocationController : ApiController
    {
        private readonly LocationRepository _repository;

        public LocationController(IDbConnector connector)
        {
            _repository = new LocationRepository(connector);
        }

        // GET api/location
        public HttpResponseMessage Get()
        {
            return Request.CreateResponse(HttpStatusCode.MovedPermanently, "/api/v1/");
        }

        // GET api/location/5
        public HttpResponseMessage Get(long id)
        {
            return Request.CreateResponse(HttpStatusCode.MovedPermanently, "/api/v1/");
        }

        // POST api/location
        public HttpResponseMessage Post([FromBody]Location value)
        {
            return Request.CreateResponse(HttpStatusCode.MovedPermanently, "/api/v1/");
        }

        // PUT api/location/5
        public HttpResponseMessage Put(long id, [FromBody]Location value)
        {
            return Request.CreateResponse(HttpStatusCode.MovedPermanently, "/api/v1/");
        }

        // DELETE api/location/5
        public HttpResponseMessage Delete(long id)
        {
            return Request.CreateResponse(HttpStatusCode.MovedPermanently, "/api/v1/");
        }

        public HttpResponseMessage Options()
        {
            return Request.CreateResponse(HttpStatusCode.MovedPermanently, "/api/v1/");
        }
    }
}
