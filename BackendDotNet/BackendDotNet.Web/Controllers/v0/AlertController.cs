﻿using BackendDotNet.Common.Entities;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using BackendDotNet.Database.Abstract;
using BackendDotNet.Database.Concrete;

namespace BackendDotNet.Web.Controllers.v0
{
    public class AlertController : ApiController
    {
        private readonly AlertRepository _repository;

        public AlertController(IDbConnector connector)
        {
            _repository = new AlertRepository(connector);
        }

        // GET api/alert
        public HttpResponseMessage Get()
        {
            return Request.CreateResponse(HttpStatusCode.MovedPermanently, "/api/v1/");
        }

        // GET api/alert/5
        public HttpResponseMessage Get(long id)
        {
            return Request.CreateResponse(HttpStatusCode.MovedPermanently, "/api/v1/");
        }

        // POST api/alert
        public HttpResponseMessage Post([FromBody]Alert value)
        {
            return Request.CreateResponse(HttpStatusCode.MovedPermanently, "/api/v1/");
        }

        // PUT api/alert/5
        public HttpResponseMessage Put(long id, [FromBody]Alert value)
        {
            return Request.CreateResponse(HttpStatusCode.MovedPermanently, "/api/v1/");
        }

        // DELETE api/alert/5
        public HttpResponseMessage Delete(long id)
        {
            return Request.CreateResponse(HttpStatusCode.MovedPermanently, "/api/v1/");
        }

        public HttpResponseMessage Options()
        {
            return Request.CreateResponse(HttpStatusCode.MovedPermanently, "/api/v1/");
        }
    }
}
