﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using BackendDotNet.Common.Entities;
using BackendDotNet.Database.Abstract;
using BackendDotNet.Database.Concrete;
using PagedList;

namespace BackendDotNet.Web.Controllers.v2
{
    [System.Web.Http.Authorize]
    public class LocationController : ApiController
    {
        private readonly LocationRepository _locationRepository;

        public LocationController(IDbConnector source)
        {
            _locationRepository = new LocationRepository(source);
        }

        public HttpResponseMessage Get(int page = 1, int count = 10)
        {
            var tmp = _locationRepository.GetAll();

            var pagedList = new PagedList<Location>(tmp, page, count);

            return Request.CreateResponse(HttpStatusCode.OK, pagedList);
        }

        public HttpResponseMessage Get(long id)
        {
            var tmp = _locationRepository.Find(id);
            if (tmp == null)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }
            return Request.CreateResponse(HttpStatusCode.OK, tmp);
        }

        public HttpResponseMessage Post(Location loc)
        {
            return Request.CreateResponse(HttpStatusCode.MethodNotAllowed);
        }

        public HttpResponseMessage Put(long id,Location loc)
        {
            return Request.CreateResponse(HttpStatusCode.MethodNotAllowed);
        }

        public HttpResponseMessage Delete(long id)
        {
            return Request.CreateResponse(HttpStatusCode.MethodNotAllowed);
        }

        /// <summary>
        /// OPTIONS /api/v1/
        /// Returns the options for this controller.
        /// </summary>
        /// <returns></returns>
        public HttpResponseMessage Options()
        {
            return Request.CreateResponse(HttpStatusCode.OK, "API: http://csci321f14.pbworks.com/w/page/87779584/Location");
        }
    }
}
