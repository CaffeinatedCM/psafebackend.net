﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Caching;
using System.Web.Http;
using BackendDotNet.Common.Entities;
using BackendDotNet.Database.Abstract;
using BackendDotNet.Database.Concrete;
using PagedList;

namespace BackendDotNet.Web.Controllers.v2
{
    [System.Web.Http.Authorize]
    public class UserController : ApiController
    {
        private readonly UserRepository _userRepository;

        public UserController(IDbConnector connector)
        {
            _userRepository = new UserRepository(connector);
        }

        public HttpResponseMessage Get(long? school = null,int page = 1, int count = 10)
        {
            var tmp = _userRepository.GetAll();

            if (school.HasValue)
            {
                tmp = tmp.Where(x => x.School.ID == school).ToList();
            }

            var pagedList = new PagedList<User>(tmp, page, count);

            return Request.CreateResponse(HttpStatusCode.OK, pagedList);
        }

        public HttpResponseMessage Get(long id)
        {
            var tmp = _userRepository.Find(id);
            if (tmp == null)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }

            return Request.CreateResponse(HttpStatusCode.OK, tmp);
        }

        public HttpResponseMessage Post(User user)
        {
            if (user.ID != 0 || !ModelState.IsValid)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }

            var tmp = _userRepository.Add(user);
            if (tmp == null)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }

            return Request.CreateResponse(HttpStatusCode.Created, tmp);
        }

        public HttpResponseMessage Put(long id, User user)
        {
            if (id != user.ID && user.ID != 0)
            {
                return Request.CreateResponse(HttpStatusCode.NotModified);
            }

            user.ID = id;
            var tmp = _userRepository.Update(user);
            return Request.CreateResponse(HttpStatusCode.OK, tmp);
        }

        public HttpResponseMessage Delete(long id)
        {
            return Request.CreateResponse(HttpStatusCode.MethodNotAllowed);
        }

        /// <summary>
        /// OPTIONS /api/v1/
        /// Returns the options for this controller.
        /// </summary>
        /// <returns></returns>
        public HttpResponseMessage Options()
        {
            return Request.CreateResponse(HttpStatusCode.OK, "API: http://csci321f14.pbworks.com/w/page/87000367/User");
        }
    }
}
