﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Caching;
using System.Web.Http;
using BackendDotNet.Common.Entities;
using BackendDotNet.Database.Abstract;
using BackendDotNet.Database.Concrete;
using PagedList;

namespace BackendDotNet.Web.Controllers.v1
{
    public class SchoolController : ApiController
    {
        private readonly SchoolRepository _schoolRepository;

        public SchoolController(IDbConnector connector)
        {
            _schoolRepository = new SchoolRepository(connector);
        }

        public HttpResponseMessage Get(int page = 1, int count = 10)
        {
            var tmp = _schoolRepository.GetAll();

            var pagedList = new PagedList<School>(tmp, page, count);

            return Request.CreateResponse(HttpStatusCode.OK, pagedList);
        }

        public HttpResponseMessage Get(long id)
        {
            var tmp = _schoolRepository.Find(id);
            if (tmp == null)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }
            return Request.CreateResponse(HttpStatusCode.OK, tmp);
        }

        public HttpResponseMessage Post(School school)
        {
            if (school.ID != 0)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }

            if (ModelState.IsValid)
            {
                var tmp = _schoolRepository.Add(school);
                return Request.CreateResponse(HttpStatusCode.Created, tmp);
            }
            return Request.CreateResponse(HttpStatusCode.BadRequest,ModelState);
        }

        public HttpResponseMessage Put(long id, School school)
        {
            var tmp = _schoolRepository.Find(id);
            if (tmp == null)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }

            if (ModelState.IsValid)
            {
                tmp = school;
                var result = _schoolRepository.Update(tmp);
                return Request.CreateResponse(HttpStatusCode.OK, result);
            }
            return Request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
        }

        public HttpResponseMessage Delete(long id)
        {
            return Request.CreateResponse(HttpStatusCode.MethodNotAllowed);
        }

        /// <summary>
        /// OPTIONS /api/v1/
        /// Returns the options for this controller.
        /// </summary>
        /// <returns></returns>
        public HttpResponseMessage Options()
        {
            return Request.CreateResponse(HttpStatusCode.OK, "API: http://csci321f14.pbworks.com/w/page/87779575/School");
        }
    }
}
