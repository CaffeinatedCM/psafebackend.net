﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using BackendDotNet.Common.Entities;
using BackendDotNet.Database.Abstract;
using BackendDotNet.Database.Concrete;
using BackendDotNet.Models;
using PagedList;

namespace BackendDotNet.Controllers.v2
{
    public class ReportController : ApiController
    {
        private readonly ReportRepository _reportRepository;
        private readonly StatusRepository _statusRepository;
        private IDbConnector _connector;

        public ReportController(IDbConnector connector)
        {
            _reportRepository = new ReportRepository(connector);
            _statusRepository = new StatusRepository(connector);
            _connector = connector;
        }

        [AuthorizeUser]
        public HttpResponseMessage Get(long? school = null, long? status = null, bool? flag = null, int page = 1, int count = 10)
        {
            var tmp = _reportRepository.GetAll();

            if (school.HasValue)
            {
                tmp = tmp.Where(x => x.School.ID == school).ToList();
            }

            if (status.HasValue)
            {
                tmp = tmp.Where(x => x.Status.ID == status).ToList();
            }

            if (flag.HasValue)
            {
                tmp = tmp.Where(x => x.Flag == flag).ToList();
            }

            var pagedList = new PagedList<Report>(tmp, page, count);

            return Request.CreateResponse(HttpStatusCode.OK, pagedList);
        }

        [AuthorizeUser]
        public HttpResponseMessage Get(long id)
        {
            var tmp = _reportRepository.Find(id);
            if (tmp == null)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }
            return Request.CreateResponse(HttpStatusCode.OK, tmp);
        }

        [AuthorizeUser]
        public HttpResponseMessage Post(Report report)
        {
            if (report.ID != 0 || !ModelState.IsValid)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }
            var tmp = _reportRepository.Add(report);
            if (tmp == null)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }
            return Request.CreateResponse(HttpStatusCode.Created, tmp);
        }

        [AuthorizeUser]
        public HttpResponseMessage Post(long id, string parameter, Location location)
        {
            if (parameter != "Location")
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }

            var tmp = _reportRepository.Find(id);
            if (tmp == null)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }

            tmp.Locations.Add(location);
            var tmp2 = _reportRepository.Update(tmp);
            return Request.CreateResponse(HttpStatusCode.Created, tmp2);
        }

        [AuthorizeUser]
        public HttpResponseMessage Post(long id, string parameter, Picture picture)
        {
            if (parameter != "Picture")
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }

            var tmp = _reportRepository.Find(id);
            if (tmp == null)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }

            tmp.Pictures.Add(picture);
            var tmp2 = _reportRepository.Update(tmp);
            return Request.CreateResponse(HttpStatusCode.Created, tmp2);
        }

        [AuthorizeUser]
        public HttpResponseMessage Put(long id, Report report)
        {
            var tmp = _reportRepository.Find(id);
            if (tmp != null)
            {
                tmp = report;
                _reportRepository.Update(tmp);
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            return Request.CreateResponse(HttpStatusCode.BadRequest);
        }

        [AuthorizeUser]
        public HttpResponseMessage Delete(long id)
        {
            var tmp = _reportRepository.Find(id);
            if (tmp == null)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }
            School school = tmp.School;
            foreach (Status status in _statusRepository)
            {
                if (status.School.ID == school.ID)
                {
                    if (status.Name.ToLower() == "closed")
                    {
                        tmp.Status = status;
                        _reportRepository.Update(tmp);
                        return Request.CreateResponse(HttpStatusCode.OK);
                    }
                }
            }
            return Request.CreateResponse(HttpStatusCode.BadRequest);
        }

        /// <summary>
        /// OPTIONS /api/v1/
        /// Returns the options for this controller.
        /// </summary>
        /// <returns></returns>
        public HttpResponseMessage Options()
        {
            return Request.CreateResponse(HttpStatusCode.OK, "API: http://csci321f14.pbworks.com/w/page/87000382/Report");
        }
    }
}
