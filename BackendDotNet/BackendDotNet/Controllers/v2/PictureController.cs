﻿using System.Net;
using System.Net.Http;
using System.Threading;
using System.Web.Http;
using System.Web.UI;
using BackendDotNet.Common.Entities;
using BackendDotNet.Database.Abstract;
using BackendDotNet.Database.Concrete;
using BackendDotNet.Models;
using PagedList;

namespace BackendDotNet.Controllers.v2
{
    public class PictureController : ApiController
    {
        private readonly PictureRepository _pictureRepository;
        private IDbConnector _connector;

        public PictureController(IDbConnector connector)
        {
            _pictureRepository = new PictureRepository(connector);
            _connector = connector;
        }

        [AuthorizeUser]
        public HttpResponseMessage Get(int page=1,int count = 10)
        {
            var tmp = _pictureRepository.GetAll();

            var pagedList = new PagedList<Picture>(tmp, page, count);

            return Request.CreateResponse(HttpStatusCode.OK, pagedList);
        }

        [AuthorizeUser]
        public HttpResponseMessage Get(long id)
        {
            var tmp = _pictureRepository.Find(id);
            if (tmp == null)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }
            return Request.CreateResponse(HttpStatusCode.OK, tmp);
        }

        [AuthorizeUser]
        public HttpResponseMessage Post(Picture status)
        {
            if (ModelState.IsValid && status.ID == 0)
            {
                var tmp = _pictureRepository.Add(status);

                if (tmp == null)
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError);
                }

                return Request.CreateResponse(HttpStatusCode.Created, tmp);
            }
            return Request.CreateResponse(HttpStatusCode.BadRequest);
        }

        [AuthorizeUser]
        public HttpResponseMessage Put(long id, Picture status)
        {
            return Request.CreateResponse(HttpStatusCode.MethodNotAllowed);
        }

        [AuthorizeUser]
        public HttpResponseMessage Delete(long id)
        {
            return Request.CreateResponse(HttpStatusCode.MethodNotAllowed);
        }

        /// <summary>
        /// OPTIONS /api/v1/
        /// Returns the options for this controller.
        /// </summary>
        /// <returns></returns>
        public HttpResponseMessage Options()
        {
            return Request.CreateResponse(HttpStatusCode.SeeOther, "API: http://csci321f14.pbworks.com/w/page/87000367/User");
        }
    }
}
