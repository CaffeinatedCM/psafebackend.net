﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BackendDotNet.Models
{
    public class AuthenticationUtilities
    {

        public static string GenerateToken(int length = 64)
        {
            char[] alphabet = "qwertyuiopasdfghjkklzxcvbnm1234567890QWERTYUIOPASDFGGHJKLZXCVBNM".ToCharArray();
            string tokenized_token_of_ultimate_destiny = "#";
            if (length <= 5) length = 64;
            var randy_Mc_Random = new Random();
            for (int x = 0; x < length; x++)
            {
                tokenized_token_of_ultimate_destiny += alphabet[randy_Mc_Random.Next(alphabet.Length)];
            }
            return tokenized_token_of_ultimate_destiny;
        }
    }
}