﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using BackendDotNet.Common.Entities;
using BackendDotNet.Database.Abstract;
using BackendDotNet.Database.Concrete;

namespace BackendDotNet.Models
{
    public class AuthorizeUser : ActionFilterAttribute
    {

        private readonly UserRepository _userRepository;

        public AuthorizeUser()
        {
            _userRepository = new UserRepository(new EfDbConnector());
        }

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var username = filterContext.RequestContext.HttpContext.Request.Headers.GetValues("Username").First();
            var token = filterContext.RequestContext.HttpContext.Request.Headers.GetValues("ClientSecretToken").First();
            if (username != null && token != null)
            {
                foreach (User user in _userRepository)
                {
                    if (user.Username == username)
                    {
                        if (token == user.ClientSecretToken)
                        {
                            base.OnActionExecuting(filterContext);
                        }
                    }
                }   
            }
            filterContext.Result = new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }

    }
}