﻿namespace BackendDotNet.Common.Entities
{
    public class School
    {
        public long ID { get; set; }

        public string EmailDomain { get; set; }

        public string Address { get; set; }

        public string PhoneNumber { get; set; }

        public string Name { get; set; }
    }
}