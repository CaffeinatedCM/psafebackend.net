﻿namespace BackendDotNet.Common.Entities
{
    public class Picture
    {
        public long ID { get; set; }

        public string Data { get; set; }
    }
}