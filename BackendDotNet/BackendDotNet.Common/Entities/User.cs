﻿using System.Security.Principal;

namespace BackendDotNet.Common.Entities
{
    public class User 
    {
        public long ID { get; set; }

        public string Username { get; set; }

        public string Password { get; set; }

        public Role Role { get; set; }

        public string FName { get; set; }

        public string LName { get; set; }

        public Picture Picture { get; set; }

        public School School { get; set; }

        public string OrganizationIdentifier { get; set; }

        public string ClientSecretToken { get; set; }
    }
}