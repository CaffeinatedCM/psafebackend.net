﻿using System;

namespace BackendDotNet.Common.Entities
{
    public class Location
    {
        public long ID { get; set; }

        public float Latitude { get; set; }

        public float Longitude { get; set; }

        public DateTime Time { get; set; }
    }
}