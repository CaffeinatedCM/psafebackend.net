namespace BackendDotNet.Database.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Alerts",
                c => new
                    {
                        ID = c.Long(nullable: false, identity: true),
                        Title = c.String(),
                        Priority = c.Int(nullable: false),
                        Enabled = c.Boolean(nullable: false),
                        School_ID = c.Long(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Schools", t => t.School_ID)
                .Index(t => t.School_ID);
            
            CreateTable(
                "dbo.Schools",
                c => new
                    {
                        ID = c.Long(nullable: false, identity: true),
                        EmailDomain = c.String(),
                        Address = c.String(),
                        PhoneNumber = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Locations",
                c => new
                    {
                        ID = c.Long(nullable: false, identity: true),
                        Latitude = c.Single(nullable: false),
                        Longitude = c.Single(nullable: false),
                        Time = c.DateTime(nullable: false),
                        Report_ID = c.Long(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Reports", t => t.Report_ID)
                .Index(t => t.Report_ID);
            
            CreateTable(
                "dbo.Pictures",
                c => new
                    {
                        ID = c.Long(nullable: false, identity: true),
                        Path = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Reports",
                c => new
                    {
                        ID = c.Long(nullable: false, identity: true),
                        Alert_ID = c.Long(),
                        Status_Id = c.Long(),
                        User_ID = c.Long(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Alerts", t => t.Alert_ID)
                .ForeignKey("dbo.Status", t => t.Status_Id)
                .ForeignKey("dbo.Users", t => t.User_ID)
                .Index(t => t.Alert_ID)
                .Index(t => t.Status_Id)
                .Index(t => t.User_ID);
            
            CreateTable(
                "dbo.Status",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Name = c.String(),
                        School_ID = c.Long(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Schools", t => t.School_ID)
                .Index(t => t.School_ID);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        ID = c.Long(nullable: false, identity: true),
                        Username = c.String(),
                        Password = c.String(),
                        FName = c.String(),
                        LName = c.String(),
                        OrganizationIdentifier = c.String(),
                        Picture_ID = c.Long(),
                        Role_Id = c.Long(),
                        School_ID = c.Long(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Pictures", t => t.Picture_ID)
                .ForeignKey("dbo.Roles", t => t.Role_Id)
                .ForeignKey("dbo.Schools", t => t.School_ID)
                .Index(t => t.Picture_ID)
                .Index(t => t.Role_Id)
                .Index(t => t.School_ID);
            
            CreateTable(
                "dbo.Roles",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Name = c.String(),
                        School_ID = c.Long(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Schools", t => t.School_ID)
                .Index(t => t.School_ID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Reports", "User_ID", "dbo.Users");
            DropForeignKey("dbo.Users", "School_ID", "dbo.Schools");
            DropForeignKey("dbo.Users", "Role_Id", "dbo.Roles");
            DropForeignKey("dbo.Roles", "School_ID", "dbo.Schools");
            DropForeignKey("dbo.Users", "Picture_ID", "dbo.Pictures");
            DropForeignKey("dbo.Reports", "Status_Id", "dbo.Status");
            DropForeignKey("dbo.Status", "School_ID", "dbo.Schools");
            DropForeignKey("dbo.Locations", "Report_ID", "dbo.Reports");
            DropForeignKey("dbo.Reports", "Alert_ID", "dbo.Alerts");
            DropForeignKey("dbo.Alerts", "School_ID", "dbo.Schools");
            DropIndex("dbo.Roles", new[] { "School_ID" });
            DropIndex("dbo.Users", new[] { "School_ID" });
            DropIndex("dbo.Users", new[] { "Role_Id" });
            DropIndex("dbo.Users", new[] { "Picture_ID" });
            DropIndex("dbo.Status", new[] { "School_ID" });
            DropIndex("dbo.Reports", new[] { "User_ID" });
            DropIndex("dbo.Reports", new[] { "Status_Id" });
            DropIndex("dbo.Reports", new[] { "Alert_ID" });
            DropIndex("dbo.Locations", new[] { "Report_ID" });
            DropIndex("dbo.Alerts", new[] { "School_ID" });
            DropTable("dbo.Roles");
            DropTable("dbo.Users");
            DropTable("dbo.Status");
            DropTable("dbo.Reports");
            DropTable("dbo.Pictures");
            DropTable("dbo.Locations");
            DropTable("dbo.Schools");
            DropTable("dbo.Alerts");
        }
    }
}
