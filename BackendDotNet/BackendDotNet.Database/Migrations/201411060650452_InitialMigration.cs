namespace BackendDotNet.Database.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialMigration : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.Reports", new[] { "Status_Id" });
            DropIndex("dbo.Users", new[] { "Role_Id" });
            CreateIndex("dbo.Reports", "Status_ID");
            CreateIndex("dbo.Users", "Role_ID");
        }
        
        public override void Down()
        {
            DropIndex("dbo.Users", new[] { "Role_ID" });
            DropIndex("dbo.Reports", new[] { "Status_ID" });
            CreateIndex("dbo.Users", "Role_Id");
            CreateIndex("dbo.Reports", "Status_Id");
        }
    }
}
