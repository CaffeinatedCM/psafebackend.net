namespace BackendDotNet.Database.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedFlagToReport : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Reports", "Flag", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Reports", "Flag");
        }
    }
}
