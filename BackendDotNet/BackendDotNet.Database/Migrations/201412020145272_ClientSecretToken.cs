namespace BackendDotNet.Database.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ClientSecretToken : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Users", "ClientSecretToken", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Users", "ClientSecretToken");
        }
    }
}
