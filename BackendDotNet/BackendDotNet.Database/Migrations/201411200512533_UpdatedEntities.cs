namespace BackendDotNet.Database.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdatedEntities : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Schools", "Name", c => c.String());
            AddColumn("dbo.Pictures", "Data", c => c.String());
            DropColumn("dbo.Pictures", "Path");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Pictures", "Path", c => c.String());
            DropColumn("dbo.Pictures", "Data");
            DropColumn("dbo.Schools", "Name");
        }
    }
}
