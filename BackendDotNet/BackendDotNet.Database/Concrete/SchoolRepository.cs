﻿using BackendDotNet.Common.Entities;
using BackendDotNet.Database.Abstract;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace BackendDotNet.Database.Concrete
{
    public class SchoolRepository : IRepository<School>
    {
        public SchoolRepository(IDbConnector connector)
        {
            Source = connector;
        }

        public void Dispose()
        {
            Finalize(true);
            GC.SuppressFinalize(this);
        }

        protected void Finalize(bool isDisposing)
        {
            if (isDisposing)
            {
                Source.Dispose();
            }
        }

        public IEnumerator<School> GetEnumerator()
        {
            return Source.Schools.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public IDbConnector Source { get; private set; }

        public School this[long id]
        {
            get { return Find(id); }
            set
            {
                if (Find(id) == null)
                    Add(value);
                else
                    Update(value);
            }
        }

        public School Add(School entity)
        {
            try
            {
                Source.Schools.Add(entity);
                return entity;
            }
            catch
            {
                return null;
            }
            finally
            {
                Source.SaveChanges();
            }
        }

        public School Remove(School entity)
        {
            try
            {
                Source.Schools.Remove(entity);
                return entity;
            }
            catch
            {
                return null;
            }
            finally
            {
                Source.SaveChanges();
            }
        }

        public School Find(long key)
        {
            try
            {
                return Source.Schools.First(x => x.ID == key);
            }
            catch
            {
                return null;
            }
        }

        public School Update(School entity)
        {
            try
            {
                var tmp = Source.Schools.FirstOrDefault(x => x.ID == entity.ID);
                tmp = entity;
                return tmp;
            }
            catch
            {
                return null;
            }
            finally
            {
                Source.SaveChanges();
            }
        }

        public IList<School> GetAll(Func<School, bool> filter = null)
        {
            if (filter == null)
            {
                return Source.Schools.ToList();
            }

            return Source.Schools.Where(filter.Invoke).ToList();
        }
    }
}