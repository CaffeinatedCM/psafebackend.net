﻿using System.Data.Entity;
using System.Security.Cryptography.X509Certificates;
using BackendDotNet.Common.Entities;
using BackendDotNet.Database.Abstract;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace BackendDotNet.Database.Concrete
{
    public class AlertRepository : IRepository<Alert>
    {
        public AlertRepository(IDbConnector connector)
        {
            Source = connector;
        }

        public void Dispose()
        {
            Finalize(true);
            GC.SuppressFinalize(this);
        }

        protected void Finalize(bool isDisposing)
        {
            if (isDisposing)
            {
                Source.Dispose();
            }
        }

        public IEnumerator<Alert> GetEnumerator()
        {
            return Source.Alerts.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public IDbConnector Source { get; private set; }

        public Alert this[long id]
        {
            get { return Find(id); }
            set
            {
                if (Find(id) == null)
                    Add(value);
                else
                    Update(value);
            }
        }

        public Alert Add(Alert entity)
        {
            try
            {
                var tmpSchool = Source.Schools.First(x => x.ID == entity.School.ID);
                entity.School = tmpSchool;
                Source.Alerts.Add(entity);
                return entity;
            }
            catch
            {
                return null;
            }
            finally
            {
                Source.SaveChanges();
            }
        }

        public Alert Remove(Alert entity)
        {
            try
            {
                Source.Alerts.Remove(entity);
                return entity;
            }
            catch
            {
                return null;
            }
            finally
            {
                Source.SaveChanges();
            }
        }

        public Alert Find(long key)
        {
            try
            {
                return Source.Alerts.Include(x=>x.School).First(x => x.ID == key);
            }
            catch
            {
                return null;
            }
        }

        public Alert Update(Alert entity)
        {
            try
            {
                var tmp = Source.Alerts.FirstOrDefault(x => x.ID == entity.ID);
                tmp.Color = entity.Color;
                tmp.Enabled = entity.Enabled;
                tmp.Priority = entity.Priority;
                tmp.Title = entity.Title;
                return tmp;
            }
            catch
            {
                return null;
            }
            finally
            {
                Source.SaveChanges();
            }
        }

        public IList<Alert> GetAll(Func<Alert, bool> filter = null)
        {
            if (filter == null)
            {
                return Source.Alerts.Include(x=>x.School).ToList();
            }

            return Source.Alerts.Include(x=>x.School).Where(filter.Invoke).ToList();
        }
    }
}