﻿using BackendDotNet.Common.Entities;
using BackendDotNet.Database.Abstract;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace BackendDotNet.Database.Concrete
{
    public class LocationRepository : IRepository<Location>
    {
        public LocationRepository(IDbConnector connector)
        {
            Source = connector;
        }

        public void Dispose()
        {
            Finalize(true);
            GC.SuppressFinalize(this);
        }

        protected void Finalize(bool isDisposing)
        {
            if (isDisposing)
            {
                Source.Dispose();
            }
        }

        public IEnumerator<Location> GetEnumerator()
        {
            return Source.Locations.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public IDbConnector Source { get; private set; }

        public Location this[long id]
        {
            get { return Find(id); }
            set
            {
                if (Find(id) == null)
                    Add(value);
                else
                    Update(value);
            }
        }

        public Location Add(Location entity)
        {
            try
            {
                Source.Locations.Add(entity);
                return entity;
            }
            catch
            {
                return null;
            }
            finally
            {
                Source.SaveChanges();
            }
        }

        public Location Remove(Location entity)
        {
            try
            {
                Source.Locations.Remove(entity);
                return entity;
            }
            catch
            {
                return null;
            }
            finally
            {
                Source.SaveChanges();
            }
        }

        public Location Find(long key)
        {
            try
            {
                return Source.Locations.First(x => x.ID == key);
            }
            catch
            {
                return null;
            }
        }

        public Location Update(Location entity)
        {
            try
            {
                var tmp = Source.Locations.FirstOrDefault(x => x.ID == entity.ID);
                tmp = entity;
                return tmp;
            }
            catch
            {
                return null;
            }
            finally
            {
                Source.SaveChanges();
            }
        }

        public IList<Location> GetAll(Func<Location, bool> filter = null)
        {
            if (filter == null)
            {
                return Source.Locations.ToList();
            }

            return Source.Locations.Where(filter.Invoke).ToList();
        }
    }
}