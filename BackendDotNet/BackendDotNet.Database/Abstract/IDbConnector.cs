﻿using System.Data.Entity;
using BackendDotNet.Common.Entities;
using System;
using System.Collections.Generic;

namespace BackendDotNet.Database.Abstract
{
    public interface IDbConnector : IDisposable
    {
        IDbSet<Alert> Alerts { get; }

        IDbSet<Location> Locations { get; }

        IDbSet<Picture> Pictures { get; }

        IDbSet<Report> Reports { get; }

        IDbSet<School> Schools { get; }

        IDbSet<User> Users { get; }

        IDbSet<Role> Roles { get; }

        IDbSet<Status> Statuses { get; } 

        int SaveChanges();
    }
}